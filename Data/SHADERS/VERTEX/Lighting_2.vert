[VERTEX]
#version 410
layout(location=0) in vec4 Position;
layout(location=1) in vec4 Normal;
layout(location=2) in vec2 TexCoord;
layout(location=3) in vec4 Tangent;
out vec3 vNormal;
out vec2 vTexCoord;
out vec3 vPos;
out vec3 vTangent;
out vec3 vBiTangent;
uniform mat4 ProjectionView;
uniform mat4 Transform;
void main()
{
	vTangent = Tangent.xyz;
	vNormal = Normal.xyz;
	vBiTangent = cross(vTangent, vNormal);
	vTexCoord = TexCoord;
	vPos = (Transform * Position).xyz;
	gl_Position = ProjectionView * Transform * Position;
}