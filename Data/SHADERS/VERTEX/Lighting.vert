[VERTEX]
#version 410
layout(location=0) in vec4 Position;
layout(location=1) in vec4 Normal;
layout(location=2) in vec2 TexCoord;
out vec3 vNormal;
out vec2 vTexCoord;
out vec3 vPos;
out vec3 oPos;
uniform mat4 ProjectionView;
uniform mat4 Transform;
void main()
{
	vNormal = Normal.xyz;
	vTexCoord = TexCoord;
	vPos = Position.xyz;
	gl_Position = ProjectionView * Transform * vec4(vPos.xyz, Position.w) ;
}