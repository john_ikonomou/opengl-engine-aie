[VERTEX]
#version 410
layout(location=0) in vec4 Position;
layout(location=1) in vec4 Normal;
layout(location=2) in vec2 TexCoord;
out vec3 vNormal;
out vec2 vTexCoord;
out vec3 vPos;
out vec3 oPos;
uniform mat4 ProjectionView;
uniform mat4 Transform;
uniform sampler2D PerlinTex;

uniform float HeightMod;

void main()
{
	vNormal = Normal.xyz;
	vTexCoord = TexCoord;
	vPos = Position.xyz;
	oPos = vPos;
	//vPos.y *= HeightMod;
	gl_Position = ProjectionView * vec4(vPos,Position.w);
}