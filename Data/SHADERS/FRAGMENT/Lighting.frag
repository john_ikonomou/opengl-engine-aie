[FRAGMENT]
#version 410
//Vertex Attributes
in vec3 vNormal;
in vec2 vTexCoord;
in vec3 vPos;

//Output Colour
out vec4 FragColour;

uniform vec3 AmbientLightColour;
//Light Information
uniform vec3 LightColour;
uniform vec3 LightPos;

//Forward Declarations
vec3 AmbientTerm(vec3 DiffuseTexColour);
vec3 SpecularTerm(vec3 a_HalfWay, vec3 a_Normal, float a_DiffColorMulti, vec3 a_SpecTexCol, float a_LightPower);

void main()
{
	
	vec3 LightDir = normalize(vPos - LightPos); //Light to vertex vector
	
	//Calculate Normals
	vec3 Normal = normalize(vNormal); 

	//Diffuse Term
	float DiffColourMultiplier = max(dot(-LightDir, Normal), 0.0);
	
	FragColour = (vec4(0,0,255,125)/255) * DiffColourMultiplier;
}