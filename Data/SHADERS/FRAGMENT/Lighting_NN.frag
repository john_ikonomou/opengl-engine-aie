[FRAGMENT]
#version 410
//Vertex Attributes
in vec3 vNormal;
in vec2 vTexCoord;
in vec3 vPos;
in vec3 oPos;
//Output Colour
out vec4 FragColour;

uniform vec3 LightColour;
uniform vec3 LightPos;

uniform float HeightMod;
//Colours
uniform float SnowFallAngle;
uniform vec4 tSnow;
uniform vec4 bSnow;
uniform float RockMaxHeight;
uniform vec4 tRock;
uniform vec4 bRock;
uniform float GrassMaxHeight;
uniform vec4 tGrass;
uniform vec4 bGrass;
uniform float SandMaxHeight;
uniform vec4 tSand;
uniform vec4 bSand;

uniform float WaterHeight;
void main()
{
	//Calculate Normals
	vec3 Normal = normalize(vNormal); 

	float RockHeight = RockMaxHeight * HeightMod;
	float GrassHeight = GrassMaxHeight * HeightMod;
	float SandHeight = SandMaxHeight * HeightMod;

	//Diffuse Term
	vec3 LightDir = normalize(oPos - LightPos); //Light to vertex vector
	float DiffColourMultiplier = max(dot(-LightDir, Normal), 0.0);

	//Heights
	float ZERO = 0.0f;
	FragColour = vec4(1,0,1,1); //Default Colour
	
	//Snow
	float Snow = (vPos.y - ZERO) / ((1.0f * HeightMod) - ZERO);
	Snow = clamp(Snow, 0, 1);
	vec4 snowColour = mix(bSnow, tSnow, Snow);

	//Rock
	float Rock = (vPos.y - GrassHeight) / (RockHeight - GrassHeight);
	Rock = clamp(Rock, 0, 1);
	vec4 rockColour = mix(bRock, tRock, Rock);

	//Grass
	float Grass = (vPos.y - SandHeight) / (GrassHeight - SandHeight);
	Grass = clamp(Grass, 0, 1);
	vec4 grassColour = mix(bGrass, tGrass, Grass);

	//Sand
	float Sand = (vPos.y - ZERO) / (SandHeight - ZERO);
	Sand = clamp(Sand, 0, 1);
	vec4 sandColour = mix(bSand, tSand, Sand);
	
	 //if(vPos.y < HeightMod)
	 //{
		//FragColour.xyz = grassColour;
	 //}
	 if(vPos.y < RockHeight && vPos.y > GrassHeight)
	 {
		FragColour = rockColour;
	 }
	 else if(vPos.y < GrassHeight && vPos.y > SandHeight)
	 {
		FragColour = grassColour;
	 }
	 else if(vPos.y < SandHeight && vPos.y > ZERO)
	 {
		FragColour = sandColour;
	 }

	 if(dot(vec3(0,1,0), Normal) > SnowFallAngle)
	 {
		if(vPos.y >= (WaterHeight*HeightMod))
		{
			FragColour = snowColour;
		}
	 }

	
	
	//FragColour = mix(colourB, colourA, (vPos.y - GrassMaxHeight/HeightMod) );

	FragColour *= DiffColourMultiplier;
	FragColour.a = 1;
}

