[FRAGMENT]
#version 410
in vec4 vNormal;
out vec4 FragColour;
void main()
{
	FragColour = vNormal;
}