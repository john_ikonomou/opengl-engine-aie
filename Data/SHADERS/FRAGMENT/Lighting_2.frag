[FRAGMENT]
#version 410
//Vertex Attributes
in vec3 vNormal;
in vec2 vTexCoord;
in vec3 vPos;
in vec3 vTangent;
in vec3 vBiTangent;
//Output Colour
out vec4 FragColour;

uniform vec3 AmbientLightColour;
//Light Information
uniform vec3 LightColour;
uniform float Brightness;
uniform vec3 LightPos;
//Camera Transform vec3
uniform vec4 CameraPos;
//Diffuse
uniform sampler2D DiffuseTex;
//Specular
uniform sampler2D SpecularTex;
uniform float SpecPower;
//Normal
uniform sampler2D NormalTex;

//Forward Declarations
mat3 TangentBiTangentNormal();
vec3 AmbientTerm(vec3 DiffuseTexColour);
vec3 SpecularTerm(vec3 a_HalfWay, vec3 a_Normal, float a_DiffColorMulti, vec3 a_SpecTexCol, float a_LightPower);

void main()
{
	vec3 CamPos = CameraPos.xyz;
	//Get TBN Matrix for Normal Calculation
	mat3 TBN = TangentBiTangentNormal();
	//Get Texels
	vec3 NormalTexture = texture(NormalTex,vTexCoord).xyz * 2 - 1;
	vec3 DiffuseTextureColour = texture(DiffuseTex, vTexCoord).rgb; //Diffuse Texture colour
	vec3 SpecularTextureColour = texture(SpecularTex, vTexCoord).rgb;
	//Calculate the ambient term
	vec3 AmbientT = AmbientTerm(DiffuseTextureColour);
	//Get a distance to the light	
	float Distance = length(LightPos - vPos);
	float LPower = (Brightness - Distance);
	vec3 LightDir = normalize(vPos - LightPos); //Light to vertex vector
	//Calculate Normals
	vec3 Normal = normalize(TBN * NormalTexture); //Normalize the normal

	//Diffuse Term
	float DiffColourMultiplier = max(dot(-LightDir, Normal), 0.0);
	vec3 DiffuseT = DiffuseTextureColour * DiffColourMultiplier * LightColour;
	
	//Specular Term
	vec3 Eye = normalize(CamPos - vPos);
	vec3 HalfWay = normalize(LightDir + Eye);
	vec3 SpecularT = SpecularTerm(HalfWay, Normal, DiffColourMultiplier, SpecularTextureColour, LPower);

	//Output Terms
	AmbientT  *= 0;
	DiffuseT  *= 1;
	SpecularT *= 0;

	FragColour = vec4(AmbientT + DiffuseT + SpecularT, 1.0f);
}

mat3 TangentBiTangentNormal()
{
	return mat3(normalize(vTangent), normalize(vBiTangent), normalize(vNormal));
}

vec3 AmbientTerm(vec3 a_DiffuseTexColour)
{
	return (AmbientLightColour * a_DiffuseTexColour);
}

vec3 SpecularTerm(vec3 a_HalfWay, vec3 a_Normal, float a_DiffColorMulti, vec3 a_SpecTexCol, float a_LightPower)
{
	float Difference = dot(a_Normal, a_HalfWay); //Get the size of the angle between the normalized View and Reflection Angle
	float Max = a_DiffColorMulti * max(Difference, 0.0); //Find out if the difference is greater than 0.
	float SpecColourMultiplier = pow(Max, SpecPower); //Raise Max to SpecPower
	vec3 Spec = a_SpecTexCol * SpecColourMultiplier * LightColour;
	return Spec;
}