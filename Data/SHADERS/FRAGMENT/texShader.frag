[FRAGMENT]
#version 410
in vec2 vTexCoord;
out vec4 FragColour;
uniform sampler2D diffuse;
void main()
{
	FragColour = texture(diffuse, vTexCoord);
}