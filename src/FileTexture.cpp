#include "FileTexture.h"

FileTexture::FileTexture(std::string FileName) : Texture()
{
	LoadTexture(FileName);
	InitializeTexture();
	stbi_image_free(m_ImageData);
}

FileTexture::~FileTexture()
{
}

void FileTexture::LoadTexture(std::string FileName)
{
	m_ImageData = stbi_load(FileName.c_str(), &m_ImageWidth, &m_ImageHeight, &m_ImageFormat, STBI_default);
}

void FileTexture::InitializeTexture()
{
	glGenTextures(1, &m_ImageHandle);
	glBindTexture(GL_TEXTURE_2D, m_ImageHandle);
	switch (m_ImageFormat)
	{
	case 1:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_ImageWidth, m_ImageHeight, 0, GL_RED, GL_UNSIGNED_BYTE, m_ImageData);
		break;
	case 2:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_ImageWidth, m_ImageHeight, 0, GL_RG, GL_UNSIGNED_BYTE, m_ImageData);
		break;
	case 3:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_ImageWidth, m_ImageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, m_ImageData);
		break;
	case 4:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_ImageWidth, m_ImageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, m_ImageData);
		break;
	}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}
