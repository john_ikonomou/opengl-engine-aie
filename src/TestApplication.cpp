#include "TestApplication.h"
#include "AIE/gl_core_4_4.h"

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include "ParticleEmitter.h"

using glm::vec3;
using glm::vec4;
using glm::mat4;



TestApplication::TestApplication() : m_camera(nullptr) 
{
	WORLDSIZE = 256;
	pOctaves = 8;
	pOffset = 32;
	pPersistance = 0.75f;
	pScale = 1.0f;
	pAmplitude = 25.0f;
	pHeight = 150.0f;

	WaterHeight = 0.4f;
	SnowFallAngle = 0.96f;
	tSnow = vec4(1);
	bSnow = vec4(0.95);
	RockMaxHeight = 1.0f;
	tRock = vec4(125, 125, 125, 255) / 255;
	bRock = vec4(100, 100, 100, 255) / 255;
	GrassMaxHeight = 0.75f;
	tGrass = vec4(0, 148, 0, 255) / 255;
	bGrass = vec4(0, 121, 0, 255) / 255;
	SandMaxHeight = 0.405f;
	tSand = vec4(255, 255, 0, 255) / 255;
	bSand = vec4(255, 136, 0, 255) / 255;
}

TestApplication::~TestApplication() 
{

}

bool TestApplication::startup() 
{
	// create a basic window
	createWindow("OverDrive Engine [DEV]", 1680, 1050);

	// create a camera
	m_camera = new FlyCamera(glm::pi<float>() * 0.25f, 16 / 9.f, 0.1f, 2000.f);
	m_camera->setLookAtFrom(vec3(10, 10, 10), vec3(0));
#pragma region SoulSpear_Setup
	////Soulspear Shader
	SSShader = new Shader();
	SSShader->LoadShaderFromFile(  "Data/SHADERS/VERTEX/Lighting_2.vert");
	SSShader->LoadShaderFromFile("Data/SHADERS/FRAGMENT/Lighting_2.frag");
	SSShader->LinkShader();
	//
	//SoulSpear
	SoulSpear = new FBXMesh("Data/MESH/Soulspear.fbx");
	//SSMaterial = new Material();
	//
	//Diffuse = new FileTexture("Data/MESH/Soulspear_Diffuse.tga");
	//SSMaterial->AddTexture(Diffuse, TextureType::Diffuse);
	//
	//Specular = new FileTexture("Data/MESH/Soulspear_Specular.tga");
	//SSMaterial->AddTexture(Specular, TextureType::Specular);
	//
	//Normal = new FileTexture("Data/MESH/Soulspear_Normal.tga");
	//SSMaterial->AddTexture(Normal, TextureType::Normal);
	
	//SoulSpear->setMaterial(SSMaterial);
	SoulSpear->setShaderProgram(SSShader); 

	//SoulSpear->m_GlobalTransform = glm::translate(vec3(8, 0, 0));
	//SoulSpear->bSphere.centre = (vec3)SoulSpear->m_GlobalTransform[3];

#pragma endregion
	////Soulspear Shader
	//tShader = new Shader();
	//tShader->LoadShaderFromFile("Data/SHADERS/VERTEX/Lighting_2.vert");
	//tShader->LoadShaderFromFile("Data/SHADERS/FRAGMENT/Lighting_2.frag");
	//tShader->LinkShader();

	//Tank = new FBXMesh("Data/MESH/EnemyTank.fbx");
	//TankMat = new Material();

	//tDiffuse = new FileTexture("Data/MESH/EnemyTank_D.tga");
	//TankMat->AddTexture(tDiffuse, TextureType::Diffuse);

	//tSpecular = new FileTexture("Data/MESH/EnemyTank_S.tga");
	//TankMat->AddTexture(tSpecular, TextureType::Specular);

	//tNormal = new FileTexture("Data/MESH/EnemyTank_N.tga");
	//TankMat->AddTexture(tNormal, TextureType::Normal);

	//Tank->setMaterial(TankMat);
	//Tank->setShaderProgram(tShader);
#pragma region Plane_Setup
	//Plane Shader
	PShader = new Shader();
	PShader->LoadShaderFromFile("Data/SHADERS/VERTEX/Lighting_NN.vert");
	PShader->LoadShaderFromFile("Data/SHADERS/FRAGMENT/Lighting_NN.frag");
	PShader->LinkShader();

	//Plane
	Perlin = new PerlinTexture(WORLDSIZE, 10, 0.65f, Toffset, 1.0f, 25.0f, false);
	Plane = new GridMesh(WORLDSIZE, Perlin->Perlin_Data, pHeight);
	Plane->setShaderProgram(PShader);
#pragma endregion
#pragma region Water_Setup
	Water = new GridMesh(WORLDSIZE, WORLDSIZE);
	wMat = new Material();

	wShader = new Shader();
	wShader->LoadShaderFromFile("Data/SHADERS/VERTEX/Lighting.vert");
	wShader->LoadShaderFromFile("Data/SHADERS/FRAGMENT/Lighting.frag");
	wShader->LinkShader();

	Water->setShaderProgram(wShader);
#pragma endregion
	Light = new PointLight(vec3(0, 512, 0), vec3(1, 1, 1), 64.0f);
#pragma region Particle System
	ParticleShader = new Shader();
	ParticleShader->LoadShaderFromFile("Data/SHADERS/VERTEX/Particles.vert");
	ParticleShader->LoadShaderFromFile("Data/SHADERS/FRAGMENT/Particles.frag");
	ParticleShader->LinkShader();
	pEmitter = new ParticleEmitter();
	pEmitter->Startup(1000, 20, 1, 10, 2, 50, 1, 0.1, vec4(1, 1, 1, 1), vec4(0, 1, 1, 1));
	pEmitter->setShaderProgram(ParticleShader);

	
#pragma endregion
	glEnable(GL_BLEND);

	return true;
}

void TestApplication::shutdown() 
{
	//////////////////////////////////////////////////////////////////////////
	// YOUR SHUTDOWN CODE HERE
	//////////////////////////////////////////////////////////////////////////
	ImGui_ImplGlfwGL3_Shutdown();
	// delete our camera and cleanup gizmos
	delete m_camera;
	// destroy our window properly
	destroyWindow();
	glDisable(GL_BLEND);
}

bool TestApplication::update(float deltaTime) 
{
	// close the application if the window closes
	if (glfwWindowShouldClose(m_window) || glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		return false;

	// update the camera's movement
	m_camera->update(deltaTime);
	if (UpdateTerrain)
	{
		delete Plane;
		delete Water;
		delete Perlin;
		Perlin = new PerlinTexture(WORLDSIZE, pOctaves, pPersistance, pOffset, pScale, pAmplitude, false);
		Plane = new GridMesh(WORLDSIZE, Perlin->Perlin_Data, pHeight);
		Plane->setShaderProgram(PShader);

		Water = new GridMesh(WORLDSIZE, WORLDSIZE);
		Water->setShaderProgram(wShader);
		//UpdateTerrain = false;
	}
	pEmitter->Update(deltaTime, m_camera->getTransform());
	//////////////////////////////////////////////////////////////////////////
	// YOUR UPDATE CODE HERE												//
	//////////////////////////////////////////////////////////////////////////
	
	//return true, else the application closes
	return true;
}

void TestApplication::draw() {
	ImGui_ImplGlfwGL3_NewFrame();

#pragma region FULLGUI
	ImGui::Begin("OverDrive GUI");
	ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.1f, 0.1f, 0.1f, 1.0f));
#pragma region DEBUGGUI
	if (ImGui::CollapsingHeader("DEBUG"))
	{
			//Clear Colour
			ImGui::ColorEdit3("Clear Colour", (float*)&m_ClearColour);
			//Light Colour
			vec3 LightColour = Light->GetLightColour();
			ImGui::ColorEdit3("Light Colour", (float*)&LightColour);
			Light->SetLightColour(LightColour);
	}
#pragma endregion DEBUGGUI
#pragma region TERRGUI
	if (ImGui::CollapsingHeader("TERRAIN"))
	{
#pragma region RENDTERR
		if (ImGui::CollapsingHeader("Terrain Render"))
		{
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.1f, 0.1f, 0.1f, 1.0f));
				ImGui::DragFloat("WaterHeight", &WaterHeight, 0.001f, 0.0f, 1.0f);
				ImGui::Separator();

					ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.5f, 0.0f, 1.0f, 1.0f));
				ImGui::DragFloat("Snow Fall Amount", &SnowFallAngle, 0.001f, -1.0f, 1.0f);
					ImGui::PopStyleColor();
				ImGui::ColorEdit3("Snow Top Color", (float*)&tSnow);
				ImGui::ColorEdit3("Snow Bottom Color", (float*)&bSnow);
				ImGui::Separator();

					ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.5f, 0.0f, 1.0f, 1.0f));
				ImGui::DragFloat("Top Layer Height", &RockMaxHeight, 0.001f, 0.0f, 1.0f);
					ImGui::PopStyleColor();
				ImGui::ColorEdit3("Top Layer Top Color", (float*)&tRock);
				ImGui::ColorEdit3("Top Layer Bottom Color", (float*)&bRock);
				ImGui::Separator();
			
					ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.5f, 0.0f, 1.0f, 1.0f));
				ImGui::DragFloat("Middle Layer Height", &GrassMaxHeight, 0.001f, 0.0f, 1.0f);
					ImGui::PopStyleColor();
				ImGui::ColorEdit3("Middle Layer Top Color"   , (float*)&tGrass);
				ImGui::ColorEdit3("Middle Layer Bottom Color", (float*)&bGrass);
				ImGui::Separator();

					ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.5f, 0.0f, 1.0f, 1.0f));
				ImGui::DragFloat("Bottom Layer Height", &SandMaxHeight, 0.001f, 0.0f, 1.0f);
					ImGui::PopStyleColor();
				ImGui::ColorEdit3("Bottom Layer Top Color", (float*)&tSand);
				ImGui::ColorEdit3("Bottom Layer Bottom Color", (float*)&bSand);
			ImGui::PopStyleColor();
		}
#pragma endregion RENDTERR	
#pragma region TERROPT
		if (ImGui::CollapsingHeader("Terrain Settings"))
		{
			ImGui::DragInt("World Size", &WORLDSIZE, 1.0f, 1, 65535);
			ImGui::DragInt("Offset", &pOffset, 1.0f, 1, INT_MAX);
			ImGui::DragInt("Octaves", &pOctaves, 1.0f, 1, 64);
			ImGui::DragFloat("Height Multiplier", &pHeight, 0.5f, 1.0f, 1024.0f);
			ImGui::DragFloat("Persistance", &pPersistance, 0.01f, 0.0f, 1.0f);
			ImGui::DragFloat("Scale", &pScale, 0.01f, 0.1f, 1000.0f);
			ImGui::DragFloat("Amplitude", &pAmplitude, 0.01f, 0.1f, 1000.0f);
			if (ImGui::RadioButton("PREVEIW", false))
			{
				UpdateTerrain = true;
			}
			else
			{
				UpdateTerrain = false;
			}
		}
#pragma endregion TERROPT
	}
#pragma endregion TERRGUI
	ImGui::PopStyleColor();
	ImGui::End();
#pragma endregion

	//SSShader->ResetTextureLocations();
	//tShader->ResetTextureLocations();
	wShader->ResetTextureLocations();
	PShader->ResetTextureLocations();

	// clear the screen for this frame
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(m_ClearColour.r, m_ClearColour.g, m_ClearColour.b, 1); //NOTE: CLEAR COLOUR
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	if (glfwGetKey(m_window, GLFW_KEY_F) == GLFW_PRESS)
	{
		Light->SetGlobalTransform((vec3)m_camera->getTransform()[3]);
	}

	//if (m_camera->FrustumCulling(&SoulSpear->bSphere))
	//{
	//	//SoulSpear Shader
	//	SSShader->SendUniform("ProjectionView",m_camera->getProjectionView());
	//	SSShader->SendUniform("CameraPos", (vec4)m_camera->getTransform()[3]);
	//	SSShader->SendUniform("SpecPower", 64.0f);
	//	SSShader->SendUniform("Brightness", Light->GetLightBrightness());
	//	SSShader->SendUniform("LightPos", Light->GetGlobalTransform());
	//	SSShader->SendUniform("LightColour", Light->GetLightColour());
	//	SSShader->SendUniform("AmbientLightColour", vec3(0.02f, 0.02f, 0.03f));
	//	SSShader->SendUniform("Transform", SoulSpear->m_GlobalTransform);
	//	SoulSpear->Draw();
	//}

	//if (m_camera->FrustumCulling(&Tank->bSphere))
	//{
	//	//Tank Shader
	//	tShader->SendUniform("ProjectionView", m_camera->getProjectionView());
	//	tShader->SendUniform("CameraPos", (vec4)m_camera->getTransform()[3]);
	//	tShader->SendUniform("SpecPower", 64.0f);
	//	tShader->SendUniform("Brightness", Light->GetLightBrightness());
	//	tShader->SendUniform("LightPos", Light->GetGlobalTransform());
	//	tShader->SendUniform("LightColour", Light->GetLightColour());
	//	tShader->SendUniform("AmbientLightColour", vec3(0.02f, 0.02f, 0.03f));
	//	tShader->SendUniform("Transform", Tank->m_GlobalTransform);
	//	Tank->Draw();
	//}
	
	//Perlin Shader
	PShader->SendUniform("ProjectionView", m_camera->getProjectionView());
	PShader->SendUniform("CameraPos", (vec4)m_camera->getTransform()[3]);
	PShader->SendUniform("SpecPower", 64.0f);
	PShader->SendUniform("Brightness", Light->GetLightBrightness());
	PShader->SendUniform("LightPos", Light->GetGlobalTransform());
	PShader->SendUniform("LightColour", Light->GetLightColour());
	PShader->SendUniform("AmbientLightColour", vec3(0));
	PShader->SendUniform("HeightMod", pHeight);
	PShader->SendUniform("WaterHeight", WaterHeight);
		//Terrain Colours
	PShader->SendUniform("SnowFallAngle", SnowFallAngle);
	PShader->SendUniform("tSnow", tSnow);
	PShader->SendUniform("bSnow", bSnow);
	PShader->SendUniform("RockMaxHeight", RockMaxHeight);
	PShader->SendUniform("tRock", tRock);
	PShader->SendUniform("bRock", bRock);
	PShader->SendUniform("GrassMaxHeight", GrassMaxHeight);
	PShader->SendUniform("tGrass", tGrass);
	PShader->SendUniform("bGrass", bGrass);
	PShader->SendUniform("SandMaxHeight", SandMaxHeight);
	PShader->SendUniform("tSand", tSand);
	PShader->SendUniform("bSand", bSand);
	Plane->Draw();

	//Water Shader
	wShader->SendUniform("ProjectionView", m_camera->getProjectionView());
	wShader->SendUniform("CameraPos", (vec4)m_camera->getTransform()[3]);
	wShader->SendUniform("SpecPower", 64.0f);
	wShader->SendUniform("Brightness", Light->GetLightBrightness());
	wShader->SendUniform("LightPos", Light->GetGlobalTransform());
	wShader->SendUniform("LightColour", Light->GetLightColour());
	wShader->SendUniform("AmbientLightColour", vec3(0));
	wShader->SendUniform("Transform", glm::translate(mat4(1), vec3(0, (WaterHeight * pHeight), 0)));
	Water->Draw();

	pEmitter->m_position = glm::vec3((WORLDSIZE / 4), pHeight, (WORLDSIZE / 4));
	pEmitter->Draw(m_camera);
	
	ImGui::Render();
	//Get window size
	int width = 0, height = 0;
	glfwGetWindowSize(m_window, &width, &height);
	//NOTE: WASTE OF FPS
}