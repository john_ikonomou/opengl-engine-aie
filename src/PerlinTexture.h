#pragma once
#include "BaseApplication.h"
#include "Texture.h"

class PerlinTexture : public Texture
{
public:
	PerlinTexture(int Dims, int Octaves, float Persistance, int a_Offset, float Scale, float Amplitude, bool CreateTexture);
	~PerlinTexture();

	float* Perlin_Data;
private:

	float IntNoise1(int x);
	float IntNoise2(int x);
	double Noise2D(float x, float y);

	float Linear_Interpolate(float min, float max, float value);
	float Cosine_Interpolate(float min, float max, float value);

	float SmoothNoise_1D(float x);
	float SmoothNoise_2D(float x, float y);

	float InterpolatedNoise(float x, float y);

	float PerlinNoise_2D(float x, float y, int Octaves, int Persistance);

	float ClampValue(float n, float lower, float upper);
};

