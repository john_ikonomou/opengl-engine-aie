#include "Light.h"

Light::Light()
{
	m_GlobalTransform = vec3(0, 0, 0);
	m_LightColour = vec3(1, 1, 1);
	m_LightBrightness = 16.0f;
}

Light::Light(vec3 a_Pos, vec3 a_LightColour, float a_LightBrightness)
{
	m_GlobalTransform = a_Pos;
	m_LightColour = a_LightColour;
	m_LightBrightness = a_LightBrightness;
}

Light::~Light()
{
}
