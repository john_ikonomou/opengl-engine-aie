#pragma once
#include "Texture.h"
#include "PerlinTexture.h"

enum TextureType
{
	Diffuse = 0,
	Specular,
	Normal,
	Perlin
};

class Material
{
public:
	Material();
	~Material();

	void AddTexture(Texture* a_Texture, int a_TextureType);
	Texture* GetTexture(int a_TextureType);

private:

	Texture* m_DiffuseTexture;
	Texture* m_SpecularTexture;
	Texture* m_NormalTexture;
	Texture* m_PerlinTexture;
};

