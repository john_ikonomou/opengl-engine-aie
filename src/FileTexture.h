#pragma once
#include "Texture.h"
#include <iostream>
#include "BaseApplication.h"

class FileTexture : public Texture
{
public:
	FileTexture(std::string FileName);
	~FileTexture();

	void LoadTexture(std::string FileName);
	void InitializeTexture();
};

