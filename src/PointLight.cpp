#include "PointLight.h"

PointLight::PointLight(vec3 a_Pos, vec3 a_LightColour, float a_LightBrightness) : 
	Light(a_Pos, a_LightColour, a_LightBrightness)
{
}

PointLight::~PointLight()
{
}
