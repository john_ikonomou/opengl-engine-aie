#pragma once

#include <GLM/glm.hpp>
#include <GLM/ext.hpp>
#include <map>
#include <vector>
#include "BoundingSphere.h"
#include "BaseApplication.h"

#include "Material.h"
#include "Shader.h"

using namespace std;

//3D Graphics Data
struct GL_INFO
{
	unsigned int VAO;
	unsigned int VBO;
	unsigned int IBO;
	unsigned int IndexCount;
};

struct Mesh_Shape
{
	unsigned int* m_glData = new unsigned int[3];
	unsigned int m_Indicies;
};

class Mesh
{
public:
	Mesh();
	~Mesh();

	//Functions to be implemented
	virtual void CreateOGLBuffers() = 0;
	virtual void DestroyOGLBuffers() = 0;
	virtual void Draw() = 0;
	
	//Shader
	void setShaderProgram(Shader* a_Shader) 
		{ m_Shader = a_Shader; };
	Shader* getShaderProgram() 
		{ return m_Shader; };

	//Material
	void setMaterial(Material* a_Material) 
		{ m_Material = a_Material; };
	Material* getMaterial() 
		{ return m_Material; };
	
	//Parent Mesh
	void setParentMesh(Mesh* a_Parent) //SET
		{ m_Parent = a_Parent; };
	const Mesh* getParentMesh() //GET
		{ return m_Parent; };

	//Child Mesh
	void addChildMesh(Mesh* a_Child, string a_ChildName); //SET
	const Mesh* getChildMesh(string MeshName) //GET
		{ return m_Children[MeshName]; };

	BoundingSphere bSphere;

	//Transforms
	glm::mat4 m_LocalTransform;
	glm::mat4 m_GlobalTransform;
protected:

	Shader* m_Shader;
	Material* m_Material;

	map<int, Mesh_Shape*> m_ShapeList;

	//Parent/Children
	Mesh* m_Parent;
	map<string, Mesh*> m_Children;
};

