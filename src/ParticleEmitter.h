#pragma once
#include "TestApplication.h"
#include "Camera.h"
#include "Shader.h"

class ParticleEmitter 
{
public:

	struct Particle {
		glm::vec3 position;
		glm::vec3 velocity;
		glm::vec4 colour;
		float size;
		float lifetime;
		float lifespan;
	};

	struct ParticleVertex {
		glm::vec4 position;
		glm::vec4 colour;
	};

	ParticleEmitter();

	virtual ~ParticleEmitter();

	void Startup(unsigned int a_maxParticles, unsigned int a_emitRate,
		float a_lifetimeMin, float a_lifetimeMax, float a_velocityMin,
		float a_velocityMax, float a_startSize, float a_endSize,
		const glm::vec4& a_startColour, const glm::vec4& a_endColour);

	void Emit();

	void Update(float a_deltaTime, const glm::mat4& a_cameraTransform);

	void Draw(BaseCamera* _camera);

	//Shader
	void setShaderProgram(Shader* a_Shader)
	{ m_Shader = a_Shader; };
	Shader* getShaderProgram()
	{ return m_Shader; };


	glm::vec3 m_position;
protected:
	bool m_hasStarted;
	int m_programID;

	Shader* m_Shader;
	Particle * m_Particles;
	unsigned int m_FirstDead;
	unsigned int m_MaxParticles;

	unsigned int m_VAO, m_VBO, m_ibo;
	ParticleVertex* m_VertexData;

	float m_EmitTimer;
	float m_EmitRate;
	float m_LifespanMin;
	float m_LifespanMax;
	float m_VelocityMin;
	float m_VelocityMax;
	float m_StartSize;
	float m_EndSize;
	glm::vec4 m_StartColour;
	glm::vec4 m_EndColour;
};

