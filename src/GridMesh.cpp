#include "GridMesh.h"

GridMesh::GridMesh(unsigned int a_Rows, unsigned int a_Cols)
{
	GenerateGrid(a_Rows, a_Cols);
	CreateOGLBuffers();
	CleanGrid();
}

GridMesh::GridMesh(unsigned int Dims, float* PerlinData, float heightMod)
{
	GenerateGridFromHeightmap(Dims, PerlinData, heightMod);
	CreateOGLBuffers();
	CleanGrid();
}

GridMesh::~GridMesh()
{

}

void GridMesh::GenerateGrid(unsigned int a_Rows, unsigned int a_Cols)
{
	m_Rows = a_Rows;
	m_Cols = a_Cols;
	aoVertices = new Vertex[m_Rows * m_Cols];

	Shape = new Mesh_Shape();
	Shape->m_Indicies = (m_Rows - 1) * (m_Cols - 1) * 6;

	for (unsigned int r = 0; r < m_Rows; ++r)
	{
		for (unsigned int c = 0; c < m_Cols; ++c)
		{
			//Position
			aoVertices[r * m_Cols + c].Position = vec4((float)c/2, 0, (float)r/2, 1);
			//Normals
			aoVertices[r * m_Cols + c].Normals = vec4(0, 1, 0, 1);
			//TexCoords
			aoVertices[r * m_Cols + c].TexCoord.x = ((float)r / (m_Rows - 1));
			aoVertices[r * m_Cols + c].TexCoord.y = ((float)c / (m_Cols - 1));
		}
	}

	auiIndices = new unsigned int[(m_Rows - 1) * (m_Cols - 1) * 6];
	unsigned int index = 0;
	for (unsigned int R = 0; R < (m_Rows - 1); ++R)
	{
		for (unsigned int C = 0; C < (m_Cols - 1); ++C)
		{
			//Triangle 1
			auiIndices[index++] = R * m_Cols + C;
			auiIndices[index++] = (R + 1) * m_Cols + C;
			auiIndices[index++] = (R + 1) * m_Cols + (C + 1);
			//Triangle 2		
			auiIndices[index++] = R * m_Cols + C;
			auiIndices[index++] = (R + 1) * m_Cols + (C + 1);
			auiIndices[index++] = R * m_Cols + (C + 1);
		}
	}
}

void GridMesh::GenerateGridFromHeightmap(unsigned int Dims, float* PerlinData, float heightMod)
{
	m_Rows = Dims;
	m_Cols = Dims;
	aoVertices = new Vertex[m_Rows * m_Cols];

	Shape = new Mesh_Shape();
	Shape->m_Indicies = (m_Rows - 1) * (m_Cols - 1) * 6;

	for (unsigned int r = 0; r < m_Rows; ++r)
	{
		for (unsigned int c = 0; c < m_Cols; ++c)
		{
			//Position
			aoVertices[r * m_Cols + c].Position = vec4((float)c/2, PerlinData[r * m_Cols + c] * heightMod, (float)r/2, 1);
			//Normals
			aoVertices[r * m_Cols + c].Normals = vec4(0, 1, 0, 0);
			//TexCoords
			aoVertices[r * m_Cols + c].TexCoord.x = ((float)r / (m_Rows - 1));
			aoVertices[r * m_Cols + c].TexCoord.y = ((float)c / (m_Cols - 1));
		}
	}

	auiIndices = new unsigned int[Shape->m_Indicies];
	unsigned int index = 0;
	for (unsigned int R = 0; R < (m_Rows - 1); ++R)
	{
		for (unsigned int C = 0; C < (m_Cols - 1); ++C)
		{
			//Triangle 1
			auiIndices[index++] = R * m_Cols + C;
			auiIndices[index++] = (R + 1) * m_Cols + C;
			auiIndices[index++] = (R + 1) * m_Cols + (C + 1);
			//Triangle 2		
			auiIndices[index++] = R * m_Cols + C;
			auiIndices[index++] = (R + 1) * m_Cols + (C + 1);
			auiIndices[index++] = R * m_Cols + (C + 1);
		}
	} 

	////Normals
	//for (int i = 0; i < Shape->m_Indicies; ++i)
	//{
	//	Vertex& Vert1 = aoVertices[auiIndices[i++]];
	//	Vertex& Vert2 = aoVertices[auiIndices[i++]];
	//	Vertex& Vert3 = aoVertices[auiIndices[i]];

	//	vec4 V21 = Vert2.Position - Vert1.Position;
	//	V21 = glm::normalize(V21);

	//	vec4 V31 = Vert3.Position - Vert1.Position;
	//	V31 = glm::normalize(V31);

	//	vec3 Norm = glm::cross(vec3(V21), vec3(V31));
	//	Norm = glm::normalize(Norm);
	//	Vert1.Normals = Vert2.Normals = Vert3.Normals = vec4(Norm, 0);
	//}

	//Normals
	for (int x = 1; x < Dims - 1; ++x)
	{
		for (int y = 1; y < Dims - 1; ++y)
		{
			vec3 current = vec3(aoVertices[y* Dims + x].Position);
			vec3 upV = vec3(aoVertices[(y - 1)* Dims + x].Position);
			vec3 downV = vec3(aoVertices[(y + 1)* Dims + x].Position);
			vec3 leftV = vec3(aoVertices[y* Dims + (x - 1)].Position);
			vec3 rightV = vec3(aoVertices[y* Dims + (x + 1)].Position);

			vec3 upD = upV - current;
			vec3 downD = downV - current;
			vec3 rightD = rightV - current;
			vec3 leftD = leftV - current;


			aoVertices[y* Dims + x].Normals = vec4(glm::normalize(glm::cross(rightD, upD) + glm::cross(upD, leftD) +
				glm::cross(leftD, downD) + glm::cross(downD, rightD)),0);

		}

	}
}

void GridMesh::CleanGrid()
{
	delete[] aoVertices;
	delete[] auiIndices;
}

void GridMesh::CreateOGLBuffers()
{
		unsigned int* glData = new unsigned int[3];

		glGenVertexArrays(1, &glData[0]);
		glBindVertexArray(glData[0]);
		glGenBuffers(1, &glData[1]);
		glGenBuffers(1, &glData[2]);

		glBindBuffer(GL_ARRAY_BUFFER, glData[1]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glData[2]);

		glBufferData(GL_ARRAY_BUFFER, (m_Rows * m_Cols ) * sizeof(Vertex), aoVertices, GL_STATIC_DRAW);
		
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, Shape->m_Indicies * sizeof(GLuint), auiIndices, GL_STATIC_DRAW);

		glEnableVertexAttribArray(0); // position
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);

		glEnableVertexAttribArray(1); // normal
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, sizeof(Vertex), (void*)(offsetof(Vertex, Normals)) );

		glEnableVertexAttribArray(2); // TexCoords
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(offsetof(Vertex, TexCoord)) );
		
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		Shape->m_glData = glData;
}

void GridMesh::DestroyOGLBuffers()
{

}

void GridMesh::Draw()
{
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); //NOTE: FILL/WIREFRAME MODE
	glPolygonMode(GL_FRONT, GL_FILL);

	m_Shader->Use();
	m_Shader->ResetTextureLocations();

	if (m_Material != nullptr)
	{
		if (m_Material->GetTexture(TextureType::Diffuse) != nullptr)
		{
			m_Shader->SendTexture(m_Material->GetTexture(TextureType::Diffuse)->GetHandle(), "DiffuseTex");
		}
		if (m_Material->GetTexture(TextureType::Specular) != nullptr)
		{
			m_Shader->SendTexture(m_Material->GetTexture(TextureType::Specular)->GetHandle(), "SpecularTex");
		}
		if (m_Material->GetTexture(TextureType::Normal) != nullptr)
		{
			m_Shader->SendTexture(m_Material->GetTexture(TextureType::Normal)->GetHandle(), "NormalTex");
		}
		if (m_Material->GetTexture(TextureType::Perlin) != nullptr)
		{
			m_Shader->SendTexture(m_Material->GetTexture(TextureType::Perlin)->GetHandle(), "PerlinTex");
		}
	}

	unsigned int* Data = Shape->m_glData;
	glBindVertexArray(Data[0]);
	glDrawElements(GL_TRIANGLES, Shape->m_Indicies, GL_UNSIGNED_INT, 0);
}
