#pragma once

#include <STB/stb_image.h>


class Texture
{
public:
	Texture();
	~Texture();


	unsigned int GetHandle() { return m_ImageHandle; };

protected:
	int m_ImageWidth;
	int m_ImageHeight;
	int m_ImageFormat;
	unsigned char* m_ImageData;

	unsigned int m_ImageHandle;
};