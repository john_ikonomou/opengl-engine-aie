#pragma once
#include "Light.h"
class PointLight : public Light
{
public:
	PointLight(vec3 a_Pos, vec3 a_LightColour, float a_LightBrightness);
	~PointLight();
};

