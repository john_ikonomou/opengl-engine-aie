#include "Shader.h"

unsigned int Shader::m_TextureLocation = 0;

Shader::Shader() : m_ShaderProgram(0),
				   LinkedStatus(false)
{
	FragmentSource = "";
	VertexSource = "";
	TesselationSource = "";
	GeometrySource = "";
	ComputeSource = "";
}

Shader::~Shader()
{
	DestroyShader();
}

void Shader::LoadShaderFromFile(std::string File)
{
	int ST = -1;
	enum ShaderType
	{
		VERTEX,
		FRAGMENT,
		TESSELATION,
		GEOMETRY,
		COMPUTE
	};

	std::string Shader;
	std::ifstream FileStream(File, std::ios::in);

	if (!FileStream.is_open())
	{
		std::cout << "MISSING SHADER FILE: " << File << std::endl;
	}
	else
	{
		std::string Line = "";
		int i = 0;
		while (!FileStream.eof())
		{
			std::getline(FileStream, Line);
			if (i == 0)
			{
				if (Line == "[VERTEX]")
				{
					ST = ShaderType::VERTEX;
					Debug::Log("Vertex Shader: File Read Attempt");
				}
				else if (Line == "[FRAGMENT]")
				{
					ST = ShaderType::FRAGMENT;
					Debug::Log("Fragment Shader: File Read Attempt");
				}
				else if (Line == "[TESSELATION]")
				{
					ST = ShaderType::TESSELATION;
					Debug::Log("Tesselation Shader: File Read Attempt");
				}
				else if (Line == "[GEOMETRY]")
				{
					ST = ShaderType::GEOMETRY;
					Debug::Log("Geometry Shader: File Read Attempt");
				}
				else if (Line == "[COMPUTE]")
				{
					ST = ShaderType::COMPUTE;
					Debug::Log("Compute Shader: File Read Attempt");
				}
				else
				{
					Debug::Log("INVALID SHADER FILE: " + File);
				}
			}
			if (i>0)
			{
				Shader.append(Line + "\n");
			}
			i++;
		}

		switch (ST)
		{
		case VERTEX:
			VertexSource = Shader;
			Debug::Log("Vertex Shader: File Read Successfully");
			break;
		case FRAGMENT:
			FragmentSource = Shader;
			Debug::Log("Fragment Shader: File Read Successfully");
			break;
		case TESSELATION:
			TesselationSource = Shader;
			Debug::Log("Tesselation Shader: File Read Successfully");
			break;
		case GEOMETRY:
			GeometrySource = Shader;
			Debug::Log("Geometry Shader: File Read Successfully");
			break;
		case COMPUTE:
			ComputeSource = Shader;
			Debug::Log("Compute Shader: File Read Successfully");
			break;
		}
		FileStream.close();	
	}

}

void Shader::LinkShader()
{
	unsigned int vertShader = glCreateShader(GL_VERTEX_SHADER);
	unsigned int fragShader = glCreateShader(GL_FRAGMENT_SHADER);

	std::string vertShaderSrc = VertexSource;
	std::string fragShaderSrc = FragmentSource;

	int Result = GL_FALSE;
	int LogLength;

	//Compile Vertex Shader
	std::cout << "Compiling Vertex Shader" << std::endl;

	const char* VSptr = vertShaderSrc.c_str();
	glShaderSource(vertShader, 1, &VSptr, NULL);
	glCompileShader(vertShader);

	//Check Vertex Shader
	glGetShaderiv(vertShader, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(vertShader, GL_INFO_LOG_LENGTH, &LogLength);
	std::vector<char> vertexError((LogLength > 1) ? LogLength : 1);
	glGetShaderInfoLog(vertShader, LogLength, NULL, &vertexError[0]);
	std::cout << &vertexError[0] << std::endl;

	//Compile Fragment Shader
	std::cout << "Compiling Fragment Shader" << std::endl;
	const char* FSptr = fragShaderSrc.c_str();
	glShaderSource(fragShader, 1, &FSptr, NULL);
	glCompileShader(fragShader);

	//Check Fragment Shader
	glGetShaderiv(fragShader, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &LogLength);
	std::vector<char> fragError((LogLength > 1) ? LogLength : 1);
	glGetShaderInfoLog(fragShader, LogLength, NULL, &fragError[0]);
	std::cout << &fragError[0] << std::endl;

	//Linking Program
	Debug::Log("SHADER PROGRAM COMPILING");
	m_ShaderProgram = glCreateProgram();
	glAttachShader(m_ShaderProgram, vertShader);
	glAttachShader(m_ShaderProgram, fragShader);
	glLinkProgram(m_ShaderProgram);

	glGetProgramiv(m_ShaderProgram, GL_LINK_STATUS, &Result);
	glGetProgramiv(m_ShaderProgram, GL_INFO_LOG_LENGTH, &LogLength);

	std::vector<char> ProgramError((LogLength > 1) ? LogLength : 1);
	glGetProgramInfoLog(m_ShaderProgram, LogLength, NULL, &ProgramError[0]);
	std::cout << &ProgramError[0] << std::endl;

	glDeleteShader(vertShader);
	glDeleteShader(fragShader);
	LinkedStatus = true;
}

void Shader::DestroyShader()
{
	if (m_ShaderProgram > 0)
	{
		glDeleteProgram(m_ShaderProgram);
	}
}

#pragma region SendUniformOverloads
void Shader::SendUniform(char* location, int value)
{
	Use();
	glUniform1i(glGetUniformLocation(m_ShaderProgram, location), value);
}
void Shader::SendUniform(char* location, float value)
{
	Use();
	glUniform1f(glGetUniformLocation(m_ShaderProgram, location), value);
}
void Shader::SendUniform(char* location, glm::mat4& value)
{
	Use();
	glUniformMatrix4fv(glGetUniformLocation(m_ShaderProgram, location), 1, false, glm::value_ptr(value));
}
void Shader::SendUniform(char* location, const glm::mat4& value)
{
	Use();
	glUniformMatrix4fv(glGetUniformLocation(m_ShaderProgram, location), 1, false, glm::value_ptr(value));
}
void Shader::SendUniform(char* location, glm::vec4& value)
{
	Use();
	glUniform4fv(glGetUniformLocation(m_ShaderProgram, location), 1, glm::value_ptr(value));
}
void Shader::SendUniform(char* location, glm::vec3& value)
{
	Use();
	glUniform3fv(glGetUniformLocation(m_ShaderProgram, location), 1, glm::value_ptr(value));
}
void Shader::SendUniform(char* location, glm::vec2& value)
{
	Use();
	glUniform2fv(glGetUniformLocation(m_ShaderProgram, location), 1, glm::value_ptr(value));
}
void Shader::SendUniform(char* location, bool value)
{
	Use();
	glUniform1i(glGetUniformLocation(m_ShaderProgram, location), value);
}
void Shader::SendUniform(char* location, unsigned int value)
{
	Use();
	glUniform1ui(glGetUniformLocation(m_ShaderProgram, location), value);
}
#pragma endregion

void Shader::SendTexture(unsigned int TextureHandle, char* location)
{
	if (Shader::m_TextureLocation < 32)
	{
		glActiveTexture(GL_TEXTURE0 + m_TextureLocation);
		glBindTexture(GL_TEXTURE_2D, TextureHandle);
		glUniform1i(glGetUniformLocation(m_ShaderProgram, location), m_TextureLocation);
	}
	else
	{
		Debug::Log("SHADER: FAILED TO ADD TEXTURE, TEXTURE LIMIT EXCEEDED");
	}
	m_TextureLocation++;
}