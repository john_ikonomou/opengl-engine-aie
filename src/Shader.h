#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <GLM/glm.hpp>
#include <GLM/ext.hpp>
#include "BaseApplication.h"
#include <MAKRON/Debug.h>

#include "Texture.h"

class Shader
{
public:
	Shader();
	~Shader();

	void SetID(Shader* Shader) //To clone shaders
		{ m_ShaderProgram = Shader->GetID(); }; 
	unsigned int GetID() 
		{ return m_ShaderProgram; };
	//Send Uniforms
	void SendUniform(char* location, int value);
	void SendUniform(char* location, float value);
	void SendUniform(char* location, glm::mat4& value);
	void SendUniform(char* location, const glm::mat4& value);
	void SendUniform(char* location, glm::vec4& value);
	void SendUniform(char* location, glm::vec3& value);
	void SendUniform(char* location, glm::vec2& value);
	void SendUniform(char* location, bool value);
	void SendUniform(char* location, unsigned int value);
	//Send Textures
	void SendTexture(unsigned int TextureHandle, char* location);

	void LoadShaderFromFile(std::string File);
	void LinkShader();
	void DestroyShader();

	void Use() 
		{ glUseProgram(m_ShaderProgram); };
	void ResetTextureLocations() 
		{ m_TextureLocation = 0; };

	bool LinkedStatus;
private:
	unsigned int m_ShaderProgram;
	static unsigned int m_TextureLocation;

	std::string FragmentSource;
	std::string VertexSource;
	std::string TesselationSource;
	std::string GeometrySource;
	std::string ComputeSource;
};
