#include "PerlinTexture.h"
#include <cmath>
#include <MAKRON/Debug.h>
#include <GLM/glm.hpp>
#include <GLM/ext.hpp>
using namespace glm;

PerlinTexture::PerlinTexture(int Dims, int Octaves, float Persistance, int a_Offset, float Scale, float Amplitude, bool CreateTexture)
{
	Debug::Log("CREATING PERLIN");

	m_ImageWidth  = Dims;
	m_ImageHeight = Dims;
	float Offset = a_Offset;
	float minY = 9999.0f;
	float maxY = 0.0f;
	Perlin_Data = new float[Dims * Dims];
	float scale = (1.0f / Dims) * Scale;
	int octaves = Octaves;
	for (int x = 0; x < Dims; ++x)
	{
		for (int y = 0; y < Dims; ++y)
		{
			float amplitude = Amplitude;
			float persistence = 0.5f;
			Perlin_Data[y * Dims + x] = 0;
			for (int o = 0; o < octaves; ++o) 
			{ 
				float freq = powf(2, (float)o);      

				float perlin_sample = glm::perlin(vec2((float)x + Offset, (float)y + Offset) * scale * freq) * 0.5f + 0.5f;
				Perlin_Data[y * Dims + x] += perlin_sample * amplitude;
				amplitude *= persistence; 
			}
			maxY = max(maxY, Perlin_Data[y * Dims + x]);
			minY = min(minY, Perlin_Data[y * Dims + x]);
		}
	}

	for (int x = 0; x < Dims; ++x)
	{
		for (int y = 0; y < Dims; ++y)
		{
			Perlin_Data[y * Dims + x] -= minY;
			float newMax = maxY - minY;
			Perlin_Data[y * Dims + x] = (Perlin_Data[y * Dims + x] / newMax);
		}
	}


	if (CreateTexture)
	{
		glGenTextures(1, &m_ImageHandle);
		glBindTexture(GL_TEXTURE_2D, m_ImageHandle);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, m_ImageWidth, m_ImageHeight, 0, GL_RED, GL_FLOAT, Perlin_Data);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		delete Perlin_Data;
		Perlin_Data = nullptr;
	}

	Debug::Log("PERLIN CREATION COMPLETE");
}

PerlinTexture::~PerlinTexture()
{
	if (Perlin_Data != nullptr)
	{
		delete Perlin_Data;
		Perlin_Data = nullptr;
	}
}

float PerlinTexture::IntNoise1(int x)
{
	x = (x << 13) ^ x;
	return (1.0 - ((x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
}

float PerlinTexture::IntNoise2(int x)
{
	x = (x << 13) ^ x;
	return (1.0 - ((x * (x * x * 14867 + 102337) + 1129333) & 0x7fffffff) / 1237763.0);
}

double PerlinTexture::Noise2D(float x, float y)
{
	int n = x + y * 57;
	n = pow(n << 13,n);
	return (1.0 - ((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
}

float PerlinTexture::Linear_Interpolate(float min, float max, float value)
{
	return min * (1 - value) + max * value;
}

float PerlinTexture::Cosine_Interpolate(float min, float max, float value)
{
	float ft = value * 3.1415927f;
	float f = (1 - cos(ft)) * .5;

	return  min * (1 - f) + max * f;
}

float PerlinTexture::SmoothNoise_1D(float x)
{
	return IntNoise1(x) / 2 + IntNoise1(x - 1) / 4 + IntNoise1(x + 1) / 4;
}

float PerlinTexture::SmoothNoise_2D(float x, float y)
{
	float corners = (Noise2D(x - 1, y - 1) + Noise2D(x + 1, y - 1) + Noise2D(x - 1, y + 1) + Noise2D(x + 1, y + 1)) / 16;
	float sides   = (Noise2D(x - 1, y) + Noise2D(x + 1, y) + Noise2D(x, y - 1) + Noise2D(x, y + 1)) / 8;
	float center  = (Noise2D(x, y) / 4);

	return (corners + sides + center);
}

float PerlinTexture::InterpolatedNoise(float x, float y)
{
	int X = int(x);
	float fX = x - X;

	int Y = int(y);
	float fY = y - Y;

	float v1 = SmoothNoise_2D(X, Y);
	float v2 = SmoothNoise_2D(X + 1, Y);
	float v3 = SmoothNoise_2D(X, Y + 1);
	float v4 = SmoothNoise_2D(X + 1, Y + 1);

	float i1 = Cosine_Interpolate(v1, v2, fX);
	float i2 = Cosine_Interpolate(v3, v4, fX);

	return Cosine_Interpolate(i1, i2, fY);

}

float PerlinTexture::PerlinNoise_2D(float x, float y, int Octaves, int Persistance)
{
	float total = 0;
	int p = Persistance;
	int n = Octaves - 1;

	for (int i = 0; i <= n; ++i)
	{
		float frequency = pow(2,(float)i);
		float amplitude = pow(p,(float)i);

		total = total + InterpolatedNoise(x * frequency, y * frequency) * amplitude;
	}
	return total;
}

float PerlinTexture::ClampValue(float n, float lower, float upper)
{
	return n <= lower ? lower : n >= upper ? upper : n;
}

