#pragma once

#include <GLM/glm.hpp>

using namespace glm;

class Light
{
public:
	Light();
	Light(vec3 a_Pos, vec3 a_LightColour, float a_LightBrightness);
	~Light();

	void SetGlobalTransform(vec3 a_GlobalTransform) 
		{ m_GlobalTransform = a_GlobalTransform; };
	vec3 GetGlobalTransform()
		{ return m_GlobalTransform; };

	void SetLightColour(vec3 a_LightColour)
		{ m_LightColour = a_LightColour; };
	vec3 GetLightColour()
		{ return m_LightColour; };

	void SetLightBrightness(float a_LightBrightness)
		{ m_LightBrightness = a_LightBrightness; };
	float GetLightBrightness()
		{ return m_LightBrightness; };

	vec3 m_LightColour;
private:
	//Position
	vec3 m_GlobalTransform;
	//Light Properties
	float m_LightBrightness;
};

