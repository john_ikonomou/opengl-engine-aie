#include "Mesh.h"



Mesh::Mesh()
{
	m_LocalTransform = glm::translate(vec3(0));
	m_GlobalTransform = glm::translate(vec3(0));

	m_Shader = nullptr;
	m_Material = nullptr;
}

Mesh::~Mesh()
{
}

void Mesh::addChildMesh(Mesh * a_Child, string a_ChildName)
{
	if (a_Child->getParentMesh() != nullptr)
	{
		a_Child->setParentMesh(nullptr);
		a_Child->setParentMesh(this);
		assert(a_Child->getParentMesh() == this && "Failed To Set Child_Mesh_Parent");
	}
	else
	{
		a_Child->setParentMesh(this);
	}
	m_Children[a_ChildName] = a_Child;
}
