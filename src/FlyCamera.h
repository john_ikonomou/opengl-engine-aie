#pragma once
#include "Camera.h"

class FlyCamera : public BaseCamera
{
public:
	FlyCamera(float fovY, float aspectRatio, float near, float far);
	~FlyCamera();

	void setSpeed(float speed) { m_Speed = speed; }

	// Inherited via BaseCamera
	virtual void update(float deltaTime) override;

private:
	float m_Speed;
};

