#include "ParticleEmitter.h"

ParticleEmitter::ParticleEmitter()
	: m_Particles(nullptr),
	m_FirstDead(0),
	m_MaxParticles(0),
	m_position(50, 50, 50),
	m_VAO(0), m_VBO(0), m_ibo(0),
	m_VertexData(nullptr), m_hasStarted(false)
{
}


ParticleEmitter::~ParticleEmitter()
{
	delete[] m_Particles;
	delete[] m_VertexData;

	glDeleteVertexArrays(1, &m_VAO);
	glDeleteBuffers(1, &m_VBO);
	glDeleteBuffers(1, &m_ibo);
}

void ParticleEmitter::Startup(unsigned int a_maxParticles, unsigned int a_emitRate,
							  float a_lifetimeMin, float a_lifetimeMax, 
							  float a_velocityMin, float a_velocityMax, 
							  float a_startSize, float a_endSize,
							  const glm::vec4& a_startColour, const glm::vec4& a_endColour)
{
	// set up emit timers
	m_EmitTimer = 0;
	m_EmitRate = 1.0f / a_emitRate;
	// store all variables passed in
	m_StartColour = a_startColour;
	m_EndColour = a_endColour;
	m_StartSize = a_startSize;
	m_EndSize = a_endSize;
	m_VelocityMin = a_velocityMin;
	m_VelocityMax = a_velocityMax;
	m_LifespanMin = a_lifetimeMin;
	m_LifespanMax = a_lifetimeMax;
	m_MaxParticles = a_maxParticles;
	// create particle array
	m_Particles = new Particle[m_MaxParticles];
	m_FirstDead = 0;
	// create the array of vertices for the particles
	// 4 vertices per particle for a quad.
	// will be filled during update
	m_VertexData = new ParticleVertex[m_MaxParticles * 4];

	unsigned int* indexData = new unsigned int[m_MaxParticles * 6];
	for (unsigned int i = 0; i < m_MaxParticles; ++i) {
		indexData[i * 6 + 0] = i * 4 + 0;
		indexData[i * 6 + 1] = i * 4 + 1;
		indexData[i * 6 + 2] = i * 4 + 2;
		indexData[i * 6 + 3] = i * 4 + 0;
		indexData[i * 6 + 4] = i * 4 + 2;
		indexData[i * 6 + 5] = i * 4 + 3;
	}
	// create opengl buffers
	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);
	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_ibo);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, m_MaxParticles * 4 * sizeof(ParticleVertex), m_VertexData, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_MaxParticles * 6 * sizeof(unsigned int), indexData, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0); // position
	glEnableVertexAttribArray(1); // colour
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), ((char*)0) + 16);
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	delete[] indexData;
}

void ParticleEmitter::Emit()
{
	if (m_FirstDead >= m_MaxParticles)
		return;

	Particle& Particle = m_Particles[m_FirstDead++];

	Particle.position = m_position;

	Particle.lifetime = 0;
	Particle.lifespan = (rand() / (float)RAND_MAX) *
		(m_LifespanMax - m_LifespanMin) + m_LifespanMin;

	Particle.colour = m_StartColour;
	Particle.size = m_StartSize;

	float velocity = (rand() / (float)RAND_MAX) *
		(m_VelocityMax - m_VelocityMin) + m_VelocityMin;
	Particle.velocity.x = (rand() / (float)RAND_MAX) * 2 - 1;
	Particle.velocity.y = (rand() / (float)RAND_MAX) * 2 - 1;
	Particle.velocity.z = (rand() / (float)RAND_MAX) * 2 - 1;
	Particle.velocity = glm::normalize(Particle.velocity) * velocity;
}

void ParticleEmitter::Draw(BaseCamera* _camera)
{ 
	m_Shader->Use();
	m_Shader->SendUniform("projectionView", _camera->getProjectionView());

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_FirstDead * 4 * sizeof(ParticleVertex), m_VertexData);

	//draw particles
	glBindVertexArray(m_VAO);
	glDrawElements(GL_TRIANGLES, m_FirstDead * 6, GL_UNSIGNED_INT, 0);
}


void ParticleEmitter::Update(float a_deltaTime, const glm::mat4& a_cameraTransform)
{
	using glm::vec3;
	using glm::vec4;

	//Spawn Particles
	m_EmitTimer += a_deltaTime;
	while (m_EmitTimer > m_EmitRate)
	{
		Emit();
		if (m_hasStarted)
			m_EmitTimer -= m_EmitRate;
		else
		{
			m_EmitTimer = 0;
			m_hasStarted = true;
		}
	}

	unsigned int quad = 0;
	// update particles and turn live particles into billboarded quads
	for (unsigned int i = 0; i < m_FirstDead; i++) {
		Particle* particle = &m_Particles[i];
		particle->lifetime += a_deltaTime;
		if (particle->lifetime >= particle->lifespan) {
			// swap last alive with this one
			*particle = m_Particles[m_FirstDead - 1];
			m_FirstDead--;
		}
		else 
		{
			// move particle
			particle->position += (particle->velocity * a_deltaTime);
			// size particle
			particle->size = glm::mix(m_StartSize, m_EndSize, particle->lifetime / particle->lifespan);
			// colour particle
			particle->colour = glm::mix(m_StartColour, m_EndColour, particle->lifetime / particle->lifespan);
			
			// make a quad the correct size and colour
			float halfSize = particle->size * 0.5f;
			m_VertexData[quad * 4 + 0].position = vec4(halfSize, halfSize, 0, 1);
			m_VertexData[quad * 4 + 0].colour = particle->colour;

			m_VertexData[quad * 4 + 1].position = vec4(-halfSize, halfSize, 0, 1);
			m_VertexData[quad * 4 + 1].colour = particle->colour;

			m_VertexData[quad * 4 + 2].position = vec4(-halfSize, -halfSize, 0, 1);
			m_VertexData[quad * 4 + 2].colour = particle->colour;

			m_VertexData[quad * 4 + 3].position = vec4(halfSize, -halfSize, 0, 1);
			m_VertexData[quad * 4 + 3].colour = particle->colour;
			// create billboard transform
			vec3 zAxis = glm::normalize(vec3(a_cameraTransform[3]) - particle->position);
			vec3 xAxis = glm::cross(vec3(a_cameraTransform[1]), zAxis);
			vec3 yAxis = glm::cross(zAxis, xAxis);

			glm::mat4 billboard(vec4(xAxis, 0), 
								vec4(yAxis, 0), 
								vec4(zAxis, 0), 
								vec4(0, 0, 0, 1));

			m_VertexData[quad * 4 + 0].position = billboard * m_VertexData[quad * 4 + 0].position + vec4(particle->position, 0);
			m_VertexData[quad * 4 + 1].position = billboard * m_VertexData[quad * 4 + 1].position + vec4(particle->position, 0);
			m_VertexData[quad * 4 + 2].position = billboard * m_VertexData[quad * 4 + 2].position + vec4(particle->position, 0);
			m_VertexData[quad * 4 + 3].position = billboard * m_VertexData[quad * 4 + 3].position + vec4(particle->position, 0);
			++quad;
		}
	}
}