#include "Material.h"
#include <MAKRON/Debug.h>

Material::Material()
{
	m_DiffuseTexture = nullptr;
	m_SpecularTexture = nullptr;
	m_NormalTexture = nullptr;
	m_PerlinTexture = nullptr;
}


Material::~Material()
{
}

void Material::AddTexture(Texture* a_Texture, int a_TextureType)
{
	switch (a_TextureType)
	{
	case 0:
		if (m_DiffuseTexture == nullptr)
		{
			m_DiffuseTexture = a_Texture;
		}
		else
		{
			delete m_DiffuseTexture;
			m_DiffuseTexture = a_Texture;
		}
		break;
	case 1:
		if (m_SpecularTexture == nullptr)
		{
			m_SpecularTexture = a_Texture;
		}
		else
		{
			delete m_SpecularTexture;
			m_SpecularTexture = a_Texture;
		}
		break;
	case 2:
		if (m_NormalTexture == nullptr)
		{
			m_NormalTexture = a_Texture;
		}
		else
		{
			delete m_NormalTexture;
			m_NormalTexture = a_Texture;
		}
		break;
	case 3:
		if (m_PerlinTexture == nullptr)
		{
			m_PerlinTexture = a_Texture;
		}
		else
		{
			delete m_PerlinTexture;
			m_PerlinTexture = a_Texture;
		}
		break;
	default:
		Debug::Log("MATERIAL_ERROR: NOT A VALID TEXTURE TYPE");
		break;
	}
}


Texture* Material::GetTexture(int a_TextureType)
{
	switch (a_TextureType)
	{
	case 0:
		if(m_DiffuseTexture != nullptr)
			return m_DiffuseTexture;
		break;
	case 1:
		if (m_SpecularTexture != nullptr)
			return m_SpecularTexture;
		break;
	case 2:
		if (m_NormalTexture != nullptr)
			return m_NormalTexture;
		break;
	case 3:
		if (m_PerlinTexture != nullptr)
			return m_PerlinTexture;
		break;
	}
	return nullptr;
}
