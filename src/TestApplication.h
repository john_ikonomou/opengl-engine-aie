#pragma once

#include "BaseApplication.h"

// only needed for the camera picking
#include <glm/vec3.hpp>

#include "GridMesh.h"
#include "FlyCamera.h"
#include "PointLight.h"
#include "FBXMesh.h"
#include "FileTexture.h"
#include "PerlinTexture.h"
#include "Material.h"
#include "Shader.h" 

class ParticleEmitter;
class BaseCamera;

class TestApplication : public BaseApplication {
public:

	TestApplication();
	virtual ~TestApplication();

	virtual bool startup();
	virtual void shutdown();

	virtual bool update(float deltaTime);
	virtual void draw();

private:
	BaseCamera*		m_camera;

	Mesh* SoulSpear;
	Mesh* Plane;
	Mesh* Water;
	Material* wMat;

	Material* SSMaterial;
	Texture* Diffuse;
	Texture* Specular;
	Texture* Normal;

	Mesh* Tank;
	Material* TankMat;
	Texture* tDiffuse;
	Texture* tSpecular;
	Texture* tNormal;
	
	PerlinTexture* Perlin;
	int WORLDSIZE;
	int pOctaves;
	int pOffset;
	float pPersistance;
	float pScale;
	float pAmplitude;
	float pHeight;
	bool UpdateTerrain = false;
	float WaterHeight;
	float SnowFallAngle;
	vec4 tSnow;
	vec4 bSnow;
	float RockMaxHeight;
	vec4 tRock;
	vec4 bRock;
	float GrassMaxHeight;
	vec4 tGrass;
	vec4 bGrass;
	float SandMaxHeight;
	vec4 tSand;
	vec4 bSand;

	Shader* SSShader;
	Shader* tShader;
	Shader* PShader;
	Shader* wShader;
	Shader* ParticleShader;
	ParticleEmitter* pEmitter;
	Light* Light;

	vec3 m_ClearColour;

	int Toffset = 2500;

	// this is an example position for camera picking
	glm::vec3	m_pickPosition;
};