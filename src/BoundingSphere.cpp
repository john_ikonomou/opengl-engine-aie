#include "BoundingSphere.h"

void BoundingSphere::fit(std::vector<FBXVertex>& Points)
{
	glm::vec3 min(1e37f);
	glm::vec3 max(-1e37f);

	for (auto& p : Points)
	{
		if (p.position.x < min.x) min.x = p.position.x;
		if (p.position.y < min.y) min.y = p.position.y;
		if (p.position.z < min.z) min.z = p.position.z;
		if (p.position.x > max.x) max.x = p.position.x;
		if (p.position.y > max.y) max.y = p.position.y;
		if (p.position.z > max.z) max.z = p.position.z;
	}

	centre = (min + max) * 0.5f;
	radius = glm::distance(min, centre) / 2;
}
