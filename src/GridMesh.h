#pragma once

#include <GLM/glm.hpp>
#include "Mesh.h"

using namespace glm;

struct Vertex
{
	vec4 Position;
	vec4 Normals;
	vec2 TexCoord;
};

class GridMesh : public Mesh
{
public:
	GridMesh(unsigned int a_Rows, unsigned int a_Cols);
	GridMesh(unsigned int Dims, float* PerlinData, float heightMod);
	~GridMesh();

	void GenerateGrid(unsigned int a_Rows, unsigned int a_Cols);
	void GenerateGridFromHeightmap(unsigned int Dims, float* PerlinData, float heightMod);
	void CleanGrid();


	// Inherited via Mesh
	virtual void CreateOGLBuffers() override;
	virtual void DestroyOGLBuffers() override;
	virtual void Draw() override;
private:
	unsigned int m_Rows;
	unsigned int m_Cols;

	Mesh_Shape* Shape;

	Vertex* aoVertices;
	unsigned int* auiIndices;
};

