#include "Camera.h"

BaseCamera::BaseCamera(float fovY, float aspectRatio, float near, float far)
	:m_up(0,1,0),
	m_transform(1),
	m_view(1)
{
	setPerspective(fovY, aspectRatio, near, far);
}

BaseCamera::~BaseCamera()
{

}

void BaseCamera::setPerspective(float fovY, float aspectRatio, float near, float far)
{
	m_projection = glm::perspective(glm::pi<float>() * 0.25f, 16 / 9.f, 0.1f, 1000.0f);
	m_projectionView = m_projection * m_view;
}

void BaseCamera::setLookAtFrom(const glm::vec3& from, const glm::vec3& to)
{
	m_view = glm::lookAt(from, to, glm::vec3(0, 1, 0));
	m_transform = glm::inverse(m_view);
	m_projectionView = m_projection * m_view;
}


glm::vec3 BaseCamera::screenPositionToDirection(float x, float y) const {
	
	int width = 0, height = 0;
	glfwGetWindowSize(glfwGetCurrentContext(), &width, &height);

	glm::vec3 screenPos(x / width * 2 - 1, (y / height * 2 - 1) * -1, -1);

	screenPos.x /= m_projection[0][0];
	screenPos.y /= m_projection[1][1];

	return glm::normalize(m_transform * glm::vec4(screenPos, 0)).xyz();
}

glm::vec3 BaseCamera::pickAgainstPlane(float x, float y, const glm::vec4& plane) const {

	int width = 0, height = 0;
	glfwGetWindowSize(glfwGetCurrentContext(), &width, &height);

	glm::vec3 screenPos(x / width * 2 - 1, (y / height * 2 - 1) * -1, -1);

	screenPos.x /= m_projection[0][0];
	screenPos.y /= m_projection[1][1];

	glm::vec3 dir = glm::normalize(m_transform * glm::vec4(screenPos, 0)).xyz();

	float d = (plane.w - glm::dot(m_transform[3].xyz(), plane.xyz()) / glm::dot(dir, plane.xyz()));

	return m_transform[3].xyz() + dir * d;
}

bool BaseCamera::FrustumCulling(BoundingSphere* bSphere)
{
	glm::vec4 planes[6];
	getFrustumPlanes(getProjectionView(), planes);
	for (int i = 0; i < 6; i++)
	{
		float d = glm::dot(vec3(planes[i]), bSphere->centre) +
			planes[i].w;
		if (d < -bSphere->radius)
		{
			printf("Outside Of Frustum!\n");
			return false;
			break;
		}

	}
	printf("Inside Of Frustum\n");
	return true;
}

void BaseCamera::getFrustumPlanes(const glm::mat4& Transform, glm::vec4 * Planes)
{
	// right side
	Planes[0] = vec4(
		Transform[0][3] - Transform[0][0],
		Transform[1][3] - Transform[1][0],
		Transform[2][3] - Transform[2][0],
		Transform[3][3] - Transform[3][0]);
	// left side
	Planes[1] = vec4(
		Transform[0][3] + Transform[0][0],
		Transform[1][3] + Transform[1][0],
		Transform[2][3] + Transform[2][0],
		Transform[3][3] + Transform[3][0]);
	// top
	Planes[2] = vec4(
		Transform[0][3] - Transform[0][1],
		Transform[1][3] - Transform[1][1],
		Transform[2][3] - Transform[2][1],
		Transform[3][3] - Transform[3][1]);
	// bottom
	Planes[3] = vec4(
		Transform[0][3] + Transform[0][1],
		Transform[1][3] + Transform[1][1],
		Transform[2][3] + Transform[2][1],
		Transform[3][3] + Transform[3][1]);
	// far
	Planes[4] = vec4(
		Transform[0][3] - Transform[0][2],
		Transform[1][3] - Transform[1][2],
		Transform[2][3] - Transform[2][2],
		Transform[3][3] - Transform[3][2]);
	// near
	Planes[5] = vec4(
		Transform[0][3] + Transform[0][2],
		Transform[1][3] + Transform[1][2],
		Transform[2][3] + Transform[2][2],
		Transform[3][3] + Transform[3][2]);
	// plane normalisation, based on length of normal
	for (int i = 0; i < 6; i++)
	{
		float d = glm::length(vec3(Planes[i]));
		Planes[i] /= d;
	}
}
