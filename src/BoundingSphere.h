#pragma once
#include <GLM/glm.hpp>
#include <GLM/ext.hpp>
#include <FBX/FBXFile.h>

using namespace glm;

class BoundingSphere
{
public:
	BoundingSphere() : centre(0), radius(0) { }
	~BoundingSphere() { }

	void fit(std::vector<FBXVertex>& Points);

	glm::vec3 centre;
	float radius;
};

