#pragma once
#include "Mesh.h"
#include <FBX/FBXFile.h>

class FBXMesh : public Mesh
{
public:
	FBXMesh(std::string a_FileName);
	~FBXMesh();

	// Inherited via Mesh
	virtual void Draw() override;
private:
	FBXFile* m_MeshFBX;

	virtual void CreateOGLBuffers() override;
	virtual void DestroyOGLBuffers() override;
	void LoadMeshFromFile(string a_File);
};

