#include "FBXMesh.h"



FBXMesh::FBXMesh(std::string a_FileName) : Mesh()
{
	m_MeshFBX = nullptr; //Set the FBX Mesh to nullptr

	LoadMeshFromFile(a_FileName); //Load a mesh
	assert(m_MeshFBX != nullptr && "Failed to load Mesh"); //Ensure it loaded correctly
	CreateOGLBuffers(); //Create the Graphics Data
	for (auto i = 0; i < m_MeshFBX->getMeshCount(); ++i)
	{
		bSphere.fit(m_MeshFBX->getMeshByIndex(i)->m_vertices);
	}
	delete m_MeshFBX; //Delete the FBX Data once we already have all the relevant data
	m_MeshFBX = nullptr;
}

FBXMesh::~FBXMesh()
{
	DestroyOGLBuffers();
}

void FBXMesh::CreateOGLBuffers()
{
	for (unsigned int i = 0; i < m_MeshFBX->getMeshCount(); ++i)
	{
		FBXMeshNode* mesh = m_MeshFBX->getMeshByIndex(i);
		Mesh_Shape* Shape = new Mesh_Shape();
		Shape->m_Indicies = (unsigned int)mesh->m_indices.size();
		unsigned int* glData = new unsigned int[3];

		glGenVertexArrays(1, &glData[0]);
		glBindVertexArray(glData[0]);
		glGenBuffers(1, &glData[1]);
		glGenBuffers(1, &glData[2]);

		glBindBuffer(GL_ARRAY_BUFFER, glData[1]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glData[2]);

		glBufferData(GL_ARRAY_BUFFER,
			mesh->m_vertices.size() * sizeof(FBXVertex),
			mesh->m_vertices.data(), GL_STATIC_DRAW);

		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			mesh->m_indices.size() * sizeof(unsigned int),
			mesh->m_indices.data(), GL_STATIC_DRAW);

		glEnableVertexAttribArray(0); // position
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), 0);

		glEnableVertexAttribArray(1); // normal
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, sizeof(FBXVertex), ((char*)0) + FBXVertex::NormalOffset);


		glEnableVertexAttribArray(2); // texcoords
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::TexCoord1Offset);

		glEnableVertexAttribArray(3); // Tangent
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char*)0) + FBXVertex::TangentOffset);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		
		Shape->m_glData = glData;
		m_ShapeList[i] = Shape;
	}
}

void FBXMesh::DestroyOGLBuffers()
{
}

void FBXMesh::LoadMeshFromFile(string a_File)
{
	m_MeshFBX = new FBXFile();
	m_MeshFBX->load(a_File.c_str());
	m_MeshFBX->initialiseOpenGLTextures();
}

void FBXMesh::Draw()
{
	glPolygonMode(GL_FRONT, GL_FILL); //NOTE: FILL/WIREFRAME MODE
	m_Shader->Use();

	if (m_Material != nullptr)
	{
		if(m_Material->GetTexture(TextureType::Diffuse) != nullptr) 
		{ 
			m_Shader->SendTexture(m_Material->GetTexture(TextureType::Diffuse)->GetHandle(), "DiffuseTex");
		}
		if (m_Material->GetTexture(TextureType::Specular) != nullptr)
		{
			m_Shader->SendTexture(m_Material->GetTexture(TextureType::Specular)->GetHandle(), "SpecularTex");
		}
		if (m_Material->GetTexture(TextureType::Normal) != nullptr)
		{
			m_Shader->SendTexture(m_Material->GetTexture(TextureType::Normal)->GetHandle(), "NormalTex");
		}
	}

	
	for (int i = 0; i < m_ShapeList.size(); ++i)
	{
		unsigned int* Data = (unsigned int*)m_ShapeList[i]->m_glData;
		glBindVertexArray(Data[0]);
		glDrawElements(GL_TRIANGLES, m_ShapeList[i]->m_Indicies, GL_UNSIGNED_INT, 0);
	}
}
